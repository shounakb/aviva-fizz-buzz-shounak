import React from 'react';
import './Button.css';

const button = props =>{

        return (
        
           <button className={props.classname} disabled={props.disable} onClick={props.clicked}>{props.buttonTitle}</button>
        
    );
}

export default button;