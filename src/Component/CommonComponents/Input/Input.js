import React from 'react';
import './Input.css';

const input = props =>{

        return (
        <div className="Input">
            <input type="number" min="0" max="1000" onChange={event => props.changehandler(event)} value={props.initialvalue}/>
        </div>
    );
}

export default input;