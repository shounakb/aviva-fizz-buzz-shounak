import React, { Component } from 'react';

class Layout extends Component {
  render() {
    return (
      <div>
        <header className="App-header">
          <h4>Aviva Assessment</h4>
        </header>
        {this.props.children}
      </div>
      
    );
  }
}

export default Layout;
