import React, { Component } from 'react';
import Layout from '../Component/Layout';
import Input from '../Component/CommonComponents/Input/Input';
import Button from '../Component/CommonComponents/Button/Button';
import './Home.css';
class Home extends Component {
    state={
        SubmitDisabled:true,
        inputValue:'',
        list:[],
        previousDisabled:true,
        nextDisabled:true,
        FilterValue:20,
        filterStartAt:0,
        currentPage: 1,
    }
     inputChangedhandler = (event)=>{
       if(event.target.value >0 && event.target.value<1000){
       this.setState({
            SubmitDisabled:false,
            inputValue:event.target.value
       })
    }
       else{
        alert("Please Enter a Number between 0 to 1000");
        this.setState({
            SubmitDisabled:true
           })
       }
    }
    submitHandler = () =>{
        var date = new Date();
        var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
        let array = [];        
        let currentInputValue = +(this.state.inputValue) + 1; 
        for(let i=1; i < currentInputValue ; i++){  
            if(i%3===0 && i%5!==0)  
            {               
                if(days[date.getDay()]==="Wednesday"){
                    array.push("Wizz");
                }
                else{
                    array.push("Fizz");
                }
            }
            else if(i%5===0 && i%3!==0){
                if(days[date.getDay()]==="Wednesday"){
                    array.push("Wuzz");
                }
                else{
                    array.push("Buzz");
                }
            }
            else if(i%5===0 && i%3===0){
                if(days[date.getDay()]==="Wednesday"){
                    array.push("Wizz Wuzz");
                }
                else{
                    array.push("Fizz Buzz");
                }
            }
            else{
                array.push(i);
            }
        }
         this.setState({
            list:array,
            currentPage:1,
            FilterValue:20,
            filterStartAt:0,
            previousDisabled:true
        })
        
        console.log(this.state.list);
}
    previousHandler= () =>{
        if(this.state.currentPage >=2)
        {
            const beforeClickFilterstartAt= this.state.filterStartAt;
            const afterClickFilterStartAt= Number(beforeClickFilterstartAt)-Number(20);
            let beforeClick= this.state.FilterValue;
            let afterClick= Number(beforeClick)-Number(20);
            this.setState({
                filterStartAt:afterClickFilterStartAt,
                FilterValue:afterClick,
                currentPage : this.state.currentPage - 1		
            })            
        }	        
    }
    nextHandler= () =>{
        var lastPage = Math.ceil(this.state.inputValue / 20);        
        if(this.state.currentPage < lastPage)
        {
            const beforeClickFilterstartAt= this.state.filterStartAt;
            let beforeClick= this.state.FilterValue;
            this.setState({
                filterStartAt:beforeClickFilterstartAt+20,
                FilterValue:beforeClick+20,
                currentPage : this.state.currentPage + 1
            });            
        }	 	          
    } 

  render() {
    let Listforuser= this.state.list.slice(this.state.filterStartAt,this.state.FilterValue).map((value, key)=>{
        return(
            <li className={value} key={key}>{value}</li>
        );
    })
    return (
      <div>
        <Layout>
            <h4>Enter a number</h4>
            <div className="Input-Holder">
                <div className="Input-Field"><Input changehandler={this.inputChangedhandler}/></div>
                <div className="Submit"><Button classname="submit" disable={this.state.SubmitDisabled} clicked={this.submitHandler} buttonTitle="Generate"/></div>
                <div style={{clear: "both"}}></div>
            </div>
            <div className="List">
                <ul style={{listStyleType:'none'}}>
                    {Listforuser}
                </ul>
            </div>
            <div className="Previous-Next">
                <div className="Previous">
                    <Button classname="Previous" disable={this.state.filterStartAt >=2 ? false : this.state.previousDisabled} buttonTitle="Previous" clicked={this.previousHandler}/>
                </div>
                
                <div className="Next">
                    <Button classname="Next" disable={this.state.list.length>2 ?false :this.state.nextDisabled} buttonTitle="Next" clicked={this.nextHandler}/>
                </div>
                <div style={{clear: "both"}}></div>
            </div>
        </Layout>
      </div>
    );
  }
}

export default Home;
